export interface CategoryModel {
    slug: string;
    title: string;
    description: string;
    background: string;
    tags: Array<string>;
  }
  