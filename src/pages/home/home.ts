import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LearnServiceProvider } from '../../providers/learn-service/learn-service';
import { CategoryModel } from '../../shared/categorymodel';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

 time:any;
  categories : Array<CategoryModel> = new Array<CategoryModel>();
  constructor(public navCtrl: NavController,private learnService:LearnServiceProvider) {
    this.time = Date.now();
  }

  ionViewWillEnter() {
    this.learnService.getCategoriesList()
    .subscribe(data => {
      this.categories = data; console.log(data);
    });
  }
}
