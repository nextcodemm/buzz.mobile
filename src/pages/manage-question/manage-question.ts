import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { AnswerServiceProvider } from '../../providers/answer-service/answer-service';
import { Body } from '@angular/http/src/body';
import { GlobalVars } from '../../shared/globaluser';
/**
 * Generated class for the ManageQuestionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-manage-question',
  templateUrl: 'manage-question.html',
})
export class ManageQuestionPage {

  category_param: string;
  answerForm: FormGroup;
  errMess: string;
  userid: string;
  owneruserid:string;
  displayname: string;
  postid:string;

  constructor(public navCtrl: NavController,
    public answerService: AnswerServiceProvider,
    public viewCtrl: ViewController,
    private globalVars: GlobalVars,
    public navParams: NavParams, private formBuilder: FormBuilder) {

    this.category_param = navParams.get('category_param');
    console.log(this.category_param);
    this.answerForm = this.formBuilder.group({
      answer: ['', Validators.required]
    });

    let user = this.globalVars.getMyGlobalVar();
    this.userid = user[0]["userid"];
    this.displayname = user[0]["displayname"];
    this.postid = this.category_param["postid"];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ManageQuestionPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  onSubmit() {
    let postParams = {
      posttypeid: 2,
      owneruserid:  this.userid,
      owerdisplayname:  this.displayname,
      body: this.answerForm.get('answer').value,
      title: " ",
      parentid: this.postid
    }
    this.answerService.postAnswer(postParams)
      .subscribe(data => {
        { 
          this.viewCtrl.dismiss(); };
        errmess => { this.errMess = errmess; };
      });

  }
}
