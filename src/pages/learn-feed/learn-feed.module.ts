import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LearnFeedPage } from './learn-feed';

@NgModule({
  declarations: [
    LearnFeedPage,
  ],
  imports: [
    IonicPageModule.forChild(LearnFeedPage),
  ],
})
export class LearnFeedPageModule {}
