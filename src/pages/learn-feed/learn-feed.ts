import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { LearnServiceProvider } from '../../providers/learn-service/learn-service';
import { CategoryModel } from '../../shared/categorymodel';
import { LearnDetailsPage } from '../learn-details/learn-details';
import { GlobalVars } from '../../shared/globaluser';
import * as moment from 'moment';

/**
 * Generated class for the LearnFeedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-learn-feed',
  templateUrl: 'learn-feed.html',
})
export class LearnFeedPage {

  time: any;
  errMess: string;
  categories: Array<CategoryModel> = new Array<CategoryModel>();
  constructor(public navCtrl: NavController, private learnService: LearnServiceProvider, public loadingCtrl: LoadingController,
    private globalVars: GlobalVars) {
    this.time = Date.now();

    let glbal = [{
      userid: "08fa6d1d-a9e8-4b7d-9ba2-7e61b04c654e",
      displayname: "CCCCC"
    }];
    this.globalVars.setMyGlobalVar(glbal);
  }

  ionViewDidLoad() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.learnService.getCategoriesList()
      .subscribe(data => {
        { this.categories = data, loading.dismiss() };
        errmess => { this.errMess = errmess; loading.dismiss() };
      });
  }

  openDetails(params) {
    this.navCtrl.push(LearnDetailsPage, params);
  }
}
