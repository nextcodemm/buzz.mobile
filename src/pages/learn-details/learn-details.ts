import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ModalController } from 'ionic-angular';
import { GlobalVars } from '../../shared/globaluser';
import { AnswerServiceProvider } from '../../providers/answer-service/answer-service';
import { isPresent } from 'ionic-angular/util/util';

import { QuestionDetailsPage } from '../question-details/question-details';
import { ManageQuestionPage } from '../manage-question/manage-question';

/**
 * Generated class for the LearnDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-learn-details',
  templateUrl: 'learn-details.html',
})
export class LearnDetailsPage {

  userid: string;
  owneruserid: string;
  displayname: string;
  postid: string;
  answers: Array<any> = [];
  category: any;
  errMess: string;
  category_param: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private globalVars: GlobalVars,
    public answerService: AnswerServiceProvider,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController) {

    this.category_param = navParams.get('category');
    let user = this.globalVars.getMyGlobalVar();
    this.userid = user[0]["userid"];
    this.displayname = user[0]["displayname"];
    this.postid = this.category_param["postid"];
    this.owneruserid = this.category_param["owneruserid"];
    this.category = isPresent(this.category_param) ? this.category_param : null;

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LearnDetailsPage');
    this.getQuestions();

  }

  getQuestions() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.answerService.getAnswersBySlug(this.postid)
      .subscribe(data => {
        { this.answers = data, loading.dismiss(); console.log(this.answers) };
        errmess => { this.errMess = errmess; loading.dismiss() };
      });
  }

  createQuestionModal() {
    let create_question_modal = this.modalCtrl.create(ManageQuestionPage, { category_param: this.category_param });
    create_question_modal.onDidDismiss(data => {
      this.getQuestions();
    });
    create_question_modal.present();
  }

  addPositiveVote(answer) {

    let postParams = {
      owneruserid: answer['owneruserid'],
      userid: this.userid,
      postid: answer['postid']
    }
  
    this.answerService.positiveVote(postParams)
      .subscribe(data => {
        { this.getQuestions(); };
        errmess => { };
      });
  }

  addNegativeVote(answer) {

    let postParams = {
      owneruserid: answer['owneruserid'],
      userid: this.userid,
      postid: answer['postid']
    }

    this.answerService.negativeVote(postParams)
    .subscribe(data => {
      { this.getQuestions(); };
      errmess => { };
    });
  }

  openComment(answer){
    this.navCtrl.push(QuestionDetailsPage , { answer : answer});
  }
}
