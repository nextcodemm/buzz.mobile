import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController, AlertController, ModalController  } from 'ionic-angular';
import { isPresent } from 'ionic-angular/util/util';
import { CommentServiceProvider } from '../../providers/comment-service/comment-service';
import { QuestionserviceProvider } from '../../providers/questionservice/questionservice';
import { AnswerServiceProvider } from '../../providers/answer-service/answer-service';
/**
 * Generated class for the QuestionDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-question-details',
  templateUrl: 'question-details.html',
})
export class QuestionDetailsPage {

  answerId: string;
  answer_param: any;
  category: any;
  answers: Array<any> = [];
  errMess:string;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public answerService: AnswerServiceProvider,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController) {
    this.answer_param = navParams.get('answer');
    this.answerId = isPresent(this.answer_param) ? this.answer_param : null;
    this.category = isPresent(this.answer_param) ? this.answer_param : null;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuestionDetailsPage');
    this.getComment();
  }

  getComment() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.answerService.getCommentBySlug(this.answerId["postid"])
      .subscribe(data => {
        { this.answers = data ; loading.dismiss() ; console.log(this.answers)};
        errmess => { this.errMess = errmess; loading.dismiss() };
      });
  }


  createCommantModal(){
    // let create_question_modal = this.modalCtrl.create(ManageQuestionPage);
    // // create_question_modal.onDidDismiss(data => {
    // //   this.getQuestions();
    // // });
    // create_question_modal.present();
  }
}
