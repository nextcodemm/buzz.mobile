import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from "@angular/http";
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { LearnFeedPage } from '../pages/learn-feed/learn-feed';
import { LearnDetailsPage } from '../pages/learn-details/learn-details';
import { QuestionDetailsPage } from '../pages/question-details/question-details';
import { ManageQuestionPage } from '../pages/manage-question/manage-question';
import { ManageAnswerPage } from '../pages/manage-answer/manage-answer';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ProcessHttpmsgProvider } from '../providers/process-httpmsg/process-httpmsg';
import { LearnServiceProvider } from '../providers/learn-service/learn-service';

import {TimeAgoPipe} from 'time-ago-pipe';
import { baseURL } from '../shared/baseurl';
import {  GlobalVars } from '../shared/globaluser';
import { QuestionserviceProvider } from '../providers/questionservice/questionservice';
import { AnswerServiceProvider } from '../providers/answer-service/answer-service';
import { CommentServiceProvider } from '../providers/comment-service/comment-service';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    LearnFeedPage,
    LearnDetailsPage,
    QuestionDetailsPage,
    ManageQuestionPage,
    ManageAnswerPage,
    TimeAgoPipe
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    LearnFeedPage,
    LearnDetailsPage,
    QuestionDetailsPage,
    ManageQuestionPage,
    ManageAnswerPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ProcessHttpmsgProvider,
    LearnServiceProvider,
    GlobalVars,
    {provide: 'BaseURL',useValue : baseURL},
    QuestionserviceProvider,
    AnswerServiceProvider,
    CommentServiceProvider,
  ]
})
export class AppModule {}
