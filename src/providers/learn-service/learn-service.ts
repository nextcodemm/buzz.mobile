import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { baseURL } from '../../shared/baseurl';
import { ProcessHttpmsgProvider } from '../process-httpmsg/process-httpmsg';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/catch';

/*
  Generated class for the LearnServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LearnServiceProvider {

  constructor(public http: Http, private processHttpmsgService: ProcessHttpmsgProvider) {
    console.log('Hello LearnServiceProvider Provider');
  }

  getCategoriesList() {
    return this.http.get(baseURL + "posts/getallposts/1")
      .map(res => { return this.processHttpmsgService.extraData(res).posts })
      .catch(error => { return this.processHttpmsgService.handleError(error) })
  }
}
