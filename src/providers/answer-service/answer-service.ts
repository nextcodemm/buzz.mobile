import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Headers, RequestOptions } from '@angular/http';
import { baseURL } from '../../shared/baseurl';
import { ProcessHttpmsgProvider } from '../process-httpmsg/process-httpmsg';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/catch';

/*
  Generated class for the AnswerServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AnswerServiceProvider {

  constructor(public http: Http, private processHttpmsgService: ProcessHttpmsgProvider) {
    console.log('Hello LearnServiceProvider Provider');
  }


  getAnswersBySlug(postid) {
    return this.http.get(baseURL + "posts/getallanswers/" + postid)
      .map(res => { return this.processHttpmsgService.extraData(res).posts })
      .catch(error => { return this.processHttpmsgService.handleError(error) })
  }

  getCommentBySlug(postid) {
    console.log(postid);
    return this.http.get(baseURL + "/comments/" + postid)
      .map(res => { return this.processHttpmsgService.extraData(res).comments })
      .catch(error => { return this.processHttpmsgService.handleError(error) })
  }

  postAnswer(answerText) {
    var header = new Headers();
    header.append('Content-Type', 'application/json');
    header.append('Access-Control-Allow-Origin', '*');
    let options = new RequestOptions({ headers: header });

    console.log(answerText);
    return this.http
      .post(baseURL + 'posts/save/', answerText, options)
      .map(res => { })
      .catch(error => { return this.processHttpmsgService.handleError(error) })
  }

  positiveVote(postParams) {

    var header = new Headers();
    header.append('Content-Type', 'application/json');
    header.append('Access-Control-Allow-Origin', '*');
    let options = new RequestOptions({ headers: header });
  
    return this.http
      .get(baseURL + 'posts/upvotes/' + postParams["owneruserid"] + "/" + postParams["postid"] + "/" + postParams["userid"])
      .map(res => { console.log(res); })
      .catch(error => { return this.processHttpmsgService.handleError(error) })
  }

  negativeVote(postParams) {
    var header = new Headers();
    header.append('Content-Type', 'application/json');
    header.append('Access-Control-Allow-Origin', '*');
    let options = new RequestOptions({ headers: header });
  
    return this.http
      .get(baseURL + 'posts/downvotes/' + postParams["owneruserid"] + "/" + postParams["postid"] + "/" + postParams["userid"])
      .map(res => { console.log(res); })
      .catch(error => { return this.processHttpmsgService.handleError(error) })
      
  }
}






