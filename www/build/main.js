webpackJsonp([5],{

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LearnFeedPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_learn_service_learn_service__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__learn_details_learn_details__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_globaluser__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the LearnFeedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LearnFeedPage = /** @class */ (function () {
    function LearnFeedPage(navCtrl, learnService, loadingCtrl, globalVars) {
        this.navCtrl = navCtrl;
        this.learnService = learnService;
        this.loadingCtrl = loadingCtrl;
        this.globalVars = globalVars;
        this.categories = new Array();
        this.time = Date.now();
        var glbal = [{
                userid: "08fa6d1d-a9e8-4b7d-9ba2-7e61b04c654e",
                displayname: "CCCCC"
            }];
        this.globalVars.setMyGlobalVar(glbal);
    }
    LearnFeedPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        this.learnService.getCategoriesList()
            .subscribe(function (data) {
            {
                _this.categories = data, loading.dismiss();
            }
            ;
            (function (errmess) { _this.errMess = errmess; loading.dismiss(); });
        });
    };
    LearnFeedPage.prototype.openDetails = function (params) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__learn_details_learn_details__["a" /* LearnDetailsPage */], params);
    };
    LearnFeedPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-learn-feed',template:/*ion-inline-start:"/Users/user/buzz/src/pages/learn-feed/learn-feed.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Home</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="masters" style="font-family:MyanmarAngoun">\n  <ion-card class="category-concept-card" *ngFor="let category of categories" (click)="openDetails({ category: category })">\n    <ion-card-header style="background-color: #42A5F5;">\n      <h2 class="category-title">{{ category.title }}</h2>\n    </ion-card-header>\n    <ion-card-content style="padding-top: 10px;">\n      <!-- <ion-avatar item-start>\n        <img style="width:20px;height:20px;" src="assets/imgs/question1.png">\n      </ion-avatar> -->\n      <ion-row>\n        <ion-col no-padding>\n          <!-- *ngFor="let tag of category.tags" -->\n          <ion-badge class="category-tag" item-right>{{ category.tags }}</ion-badge>\n        </ion-col>\n        <div text-right class="category-time">\n          by {{ category.ownerdisplayname }}\n          <div> {{ time | timeAgo }} </div> \n        </div>\n      </ion-row>\n    </ion-card-content>\n  </ion-card>\n</ion-content>'/*ion-inline-end:"/Users/user/buzz/src/pages/learn-feed/learn-feed.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_learn_service_learn_service__["a" /* LearnServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4__shared_globaluser__["a" /* GlobalVars */]])
    ], LearnFeedPage);
    return LearnFeedPage;
}());

//# sourceMappingURL=learn-feed.js.map

/***/ }),

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LearnDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_globaluser__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_answer_service_answer_service__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular_util_util__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__question_details_question_details__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__manage_question_manage_question__ = __webpack_require__(112);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the LearnDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LearnDetailsPage = /** @class */ (function () {
    function LearnDetailsPage(navCtrl, navParams, globalVars, answerService, loadingCtrl, alertCtrl, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.globalVars = globalVars;
        this.answerService = answerService;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.answers = [];
        this.category_param = navParams.get('category');
        var user = this.globalVars.getMyGlobalVar();
        this.userid = user[0]["userid"];
        this.displayname = user[0]["displayname"];
        this.postid = this.category_param["postid"];
        this.owneruserid = this.category_param["owneruserid"];
        this.category = Object(__WEBPACK_IMPORTED_MODULE_4_ionic_angular_util_util__["l" /* isPresent */])(this.category_param) ? this.category_param : null;
    }
    LearnDetailsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LearnDetailsPage');
        this.getQuestions();
    };
    LearnDetailsPage.prototype.getQuestions = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        this.answerService.getAnswersBySlug(this.postid)
            .subscribe(function (data) {
            {
                _this.answers = data, loading.dismiss();
                console.log(_this.answers);
            }
            ;
            (function (errmess) { _this.errMess = errmess; loading.dismiss(); });
        });
    };
    LearnDetailsPage.prototype.createQuestionModal = function () {
        var _this = this;
        var create_question_modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__manage_question_manage_question__["a" /* ManageQuestionPage */], { category_param: this.category_param });
        create_question_modal.onDidDismiss(function (data) {
            _this.getQuestions();
        });
        create_question_modal.present();
    };
    LearnDetailsPage.prototype.addPositiveVote = function (answer) {
        var _this = this;
        var postParams = {
            owneruserid: answer['owneruserid'],
            userid: this.userid,
            postid: answer['postid']
        };
        this.answerService.positiveVote(postParams)
            .subscribe(function (data) {
            {
                _this.getQuestions();
            }
            ;
            (function (errmess) { });
        });
    };
    LearnDetailsPage.prototype.addNegativeVote = function (answer) {
        var _this = this;
        var postParams = {
            owneruserid: answer['owneruserid'],
            userid: this.userid,
            postid: answer['postid']
        };
        this.answerService.negativeVote(postParams)
            .subscribe(function (data) {
            {
                _this.getQuestions();
            }
            ;
            (function (errmess) { });
        });
    };
    LearnDetailsPage.prototype.openComment = function (answer) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__question_details_question_details__["a" /* QuestionDetailsPage */], { answer: answer });
    };
    LearnDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-learn-details',template:/*ion-inline-start:"/Users/user/buzz/src/pages/learn-details/learn-details.html"*/'<ion-header >\n    <ion-navbar color="primary">\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n      <ion-title>\n        <span></span>\n      </ion-title>\n    </ion-navbar>\n  </ion-header>\n  \n  <ion-content style="font-family:MyanmarAngoun">\n    <ion-row class="category-concept-details" style="background-color: #42A5F5;">\n      <ion-col no-padding col-12>\n        <h2 class="category-title">{{ category.title }}!</h2>\n      </ion-col>\n      <ion-col no-padding col-12>\n        <p class="category-description">\n          {{ category.body }}\n        </p>\n      </ion-col>\n    </ion-row>\n  \n    <div *ngIf="answers.length == 0">\n      <h3 class="answers-call-out">\n        There are no answer at the time\n      </h3>\n    </div>\n  \n    <div *ngIf="answers.length > 0">\n      <!-- <h3 class="questions-call-out">\n        <span>Find all the Questions and Answers about </span>\n        <ion-badge class="call-out-tag">{{ category.title }}</ion-badge>\n        <span class="call-out-explanation"> from the community</span>\n      </h3> -->\n  \n      <ion-list class="questions-list">\n        <ion-item class="question-item" *ngFor="let answer of answers" >\n          <ion-row>\n            <ion-col class="votes-col" col-2>\n              <button class="vote-button up-vote" ion-button clear small (click)= "addPositiveVote(answer)">\n                <ion-icon name="arrow-up"></ion-icon>\n              </button>\n              <span class="question-score" [ngClass]="{\'good-score\': (answer.score) > 0, \'no-score\': (answer.score) == 0, \'bad-score\': (answer.score) < 0}">{{ answer.score }}</span>\n              <button class="vote-button down-vote" ion-button clear small (click)= "addNegativeVote(answer)">\n                <ion-icon name="arrow-down"></ion-icon>\n              </button>\n            </ion-col>\n            <ion-col col-10>\n              <ion-row class="question-details">\n                <ion-col col-12>\n                  <h2 class="question-text">{{ answer.body }}</h2>\n                </ion-col>\n                <ion-col col-4 class="votes-details">\n                  <div class="details-wrapper" (click)="openComment(answer)">\n                    <span class="details-text">Comment</span>\n                  </div>\n                </ion-col>\n                <!-- <ion-col col-4 class="answers-details" (click)="openAnswers(question)">\n                  <div class="details-wrapper">\n                    <span class="total-answers">{{ question.answers.length }}</span>\n                    <span class="details-text">{{ (question.answers.length > 1 || question.answers.length == 0) ? \'Answers\' : \'Answer\' }}</span>\n                  </div>\n                </ion-col> -->\n                <ion-col col-4 class="actions-details" *ngIf="userid == ownderid">\n                  <button class="delete-button" ion-button small round clear icon-only color="danger" (click)="delete(answer.id)">\n                    <ion-icon name="ios-trash-outline"></ion-icon>\n                  </button>\n                </ion-col>\n              </ion-row>\n            </ion-col>\n          </ion-row>\n        </ion-item>\n      </ion-list>\n    </div>\n    <ion-fab right bottom>\n        <button ion-fab color="accent">\n          <ion-icon name="arrow-dropup"></ion-icon>\n        </button>\n        <ion-fab-list side="top">\n          <button ion-fab class="btn-answer" (click)="createQuestionModal()">\n            <ion-icon name="create"></ion-icon>\n          </button>\n        </ion-fab-list>\n      </ion-fab>\n  </ion-content>\n  \n  <!-- <ion-footer>\n    <ion-toolbar>\n      <button ion-button block (click)="createQuestionModal()" color="ask">\n        <b>Have a Question? Ask!</b>\n      </button>\n    </ion-toolbar>\n  </ion-footer> -->\n  '/*ion-inline-end:"/Users/user/buzz/src/pages/learn-details/learn-details.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__shared_globaluser__["a" /* GlobalVars */],
            __WEBPACK_IMPORTED_MODULE_3__providers_answer_service_answer_service__["a" /* AnswerServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */]])
    ], LearnDetailsPage);
    return LearnDetailsPage;
}());

//# sourceMappingURL=learn-details.js.map

/***/ }),

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QuestionDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular_util_util__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_answer_service_answer_service__ = __webpack_require__(53);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the QuestionDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var QuestionDetailsPage = /** @class */ (function () {
    function QuestionDetailsPage(navCtrl, navParams, loadingCtrl, answerService, alertCtrl, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.answerService = answerService;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.answers = [];
        this.answer_param = navParams.get('answer');
        this.answerId = Object(__WEBPACK_IMPORTED_MODULE_2_ionic_angular_util_util__["l" /* isPresent */])(this.answer_param) ? this.answer_param : null;
        this.category = Object(__WEBPACK_IMPORTED_MODULE_2_ionic_angular_util_util__["l" /* isPresent */])(this.answer_param) ? this.answer_param : null;
    }
    QuestionDetailsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad QuestionDetailsPage');
        this.getComment();
    };
    QuestionDetailsPage.prototype.getComment = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        this.answerService.getCommentBySlug(this.answerId["postid"])
            .subscribe(function (data) {
            {
                _this.answers = data;
                loading.dismiss();
                console.log(_this.answers);
            }
            ;
            (function (errmess) { _this.errMess = errmess; loading.dismiss(); });
        });
    };
    QuestionDetailsPage.prototype.createCommantModal = function () {
        // let create_question_modal = this.modalCtrl.create(ManageQuestionPage);
        // // create_question_modal.onDidDismiss(data => {
        // //   this.getQuestions();
        // // });
        // create_question_modal.present();
    };
    QuestionDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-question-details',template:/*ion-inline-start:"/Users/user/buzz/src/pages/question-details/question-details.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>\n      <span>Answer</span>\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content style="font-family:MyanmarAngoun">\n  <ion-row>\n    <ion-col padding col-12>\n      <p class="category-description">\n        {{ category.body }}\n      </p>\n    </ion-col>\n  </ion-row>\n\n  <div *ngIf="answers.length == 0">\n    <h3 class="answers-call-out">\n      There are no answer at the time\n    </h3>\n  </div>\n\n  <div *ngIf="answers.length > 0">\n\n    <ion-list class="questions-list">\n      <ion-item class="question-item" *ngFor="let answer of answers">\n        <ion-row>\n          <ion-col col-10>\n            <ion-row class="question-details">\n              <ion-col col-12>\n                <h2 class="question-text">{{ answer.text }}</h2>\n              </ion-col>\n              <ion-col col-4 class="votes-details">\n                <div class="details-wrapper" (click)="openComment(answer)">\n                  <span class="details-text">Comment</span>\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-item>\n    </ion-list>\n  </div>\n  <ion-fab right bottom>\n    <button ion-fab color="accent">\n      <ion-icon name="arrow-dropup"></ion-icon>\n    </button>\n    <ion-fab-list side="top">\n      <button ion-fab class="btn-answer" (click)="createCommantModal()">\n        <ion-icon name="create"></ion-icon>\n      </button>\n    </ion-fab-list>\n  </ion-fab>\n</ion-content>\n\n<!-- <ion-footer>\n    <ion-toolbar>\n      <button ion-button block (click)="createQuestionModal()" color="ask">\n        <b>Have a Question? Ask!</b>\n      </button>\n    </ion-toolbar>\n  </ion-footer> -->'/*ion-inline-end:"/Users/user/buzz/src/pages/question-details/question-details.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_answer_service_answer_service__["a" /* AnswerServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */]])
    ], QuestionDetailsPage);
    return QuestionDetailsPage;
}());

//# sourceMappingURL=question-details.js.map

/***/ }),

/***/ 112:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ManageQuestionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_answer_service_answer_service__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_globaluser__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the ManageQuestionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ManageQuestionPage = /** @class */ (function () {
    function ManageQuestionPage(navCtrl, answerService, viewCtrl, globalVars, navParams, formBuilder) {
        this.navCtrl = navCtrl;
        this.answerService = answerService;
        this.viewCtrl = viewCtrl;
        this.globalVars = globalVars;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.category_param = navParams.get('category_param');
        console.log(this.category_param);
        this.answerForm = this.formBuilder.group({
            answer: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]
        });
        var user = this.globalVars.getMyGlobalVar();
        this.userid = user[0]["userid"];
        this.displayname = user[0]["displayname"];
        this.postid = this.category_param["postid"];
    }
    ManageQuestionPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ManageQuestionPage');
    };
    ManageQuestionPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ManageQuestionPage.prototype.onSubmit = function () {
        var _this = this;
        var postParams = {
            posttypeid: 2,
            owneruserid: this.userid,
            owerdisplayname: this.displayname,
            body: this.answerForm.get('answer').value,
            title: " ",
            parentid: this.postid
        };
        this.answerService.postAnswer(postParams)
            .subscribe(function (data) {
            {
                _this.viewCtrl.dismiss();
            }
            ;
            (function (errmess) { _this.errMess = errmess; });
        });
    };
    ManageQuestionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-manage-question',template:/*ion-inline-start:"/Users/user/buzz/src/pages/manage-question/manage-question.html"*/'  <ion-header>\n      <ion-toolbar color="primary-pale">\n        <ion-buttons start>\n          <button ion-button (click)="dismiss()">\n            <span ion-text color="white" showWhen="ios">Cancel</span>\n            <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n          </button>\n        </ion-buttons>\n        <ion-title style="font-family:MyanmarAngoun"></ion-title>\n      </ion-toolbar>\n    </ion-header>\n  \n  <ion-content >\n    <form [formGroup]="answerForm" (ngSubmit)="onSubmit()" >\n      <ion-row>\n        <ion-col offset-1 col-10>\n          <ion-item class="textarea-item">\n            <ion-label floating>Write your answer here</ion-label>\n            <ion-textarea formControlName="answer" rows="10"></ion-textarea>\n          </ion-item>\n        </ion-col>\n        <ion-col offset-1 col-10>\n          <button ion-button block type="submit" color="primary-dark">\n            <b>Answer</b>\n          </button>\n        </ion-col>\n      </ion-row>\n    </form>\n  </ion-content>\n  '/*ion-inline-end:"/Users/user/buzz/src/pages/manage-question/manage-question.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_answer_service_answer_service__["a" /* AnswerServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_4__shared_globaluser__["a" /* GlobalVars */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */]])
    ], ManageQuestionPage);
    return ManageQuestionPage;
}());

//# sourceMappingURL=manage-question.js.map

/***/ }),

/***/ 124:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 124;

/***/ }),

/***/ 165:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/learn-details/learn-details.module": [
		305,
		4
	],
	"../pages/learn-feed/learn-feed.module": [
		304,
		3
	],
	"../pages/manage-answer/manage-answer.module": [
		306,
		2
	],
	"../pages/manage-question/manage-question.module": [
		307,
		1
	],
	"../pages/question-details/question-details.module": [
		308,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 165;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 211:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ManageAnswerPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ManageAnswerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ManageAnswerPage = /** @class */ (function () {
    function ManageAnswerPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ManageAnswerPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ManageAnswerPage');
    };
    ManageAnswerPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-manage-answer',template:/*ion-inline-start:"/Users/user/buzz/src/pages/manage-answer/manage-answer.html"*/'<!--\n  Generated template for the ManageAnswerPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header >\n    <ion-navbar color="primary">\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n      <ion-title>\n        <span>Comment</span>\n      </ion-title>\n    </ion-navbar>\n  </ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/user/buzz/src/pages/manage-answer/manage-answer.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], ManageAnswerPage);
    return ManageAnswerPage;
}());

//# sourceMappingURL=manage-answer.js.map

/***/ }),

/***/ 212:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(233);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 233:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(287);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_home_home__ = __webpack_require__(293);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_list_list__ = __webpack_require__(294);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_learn_feed_learn_feed__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_learn_details_learn_details__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_question_details_question_details__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_manage_question_manage_question__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_manage_answer_manage_answer__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_status_bar__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_splash_screen__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__providers_process_httpmsg_process_httpmsg__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__providers_learn_service_learn_service__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_time_ago_pipe__ = __webpack_require__(295);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__shared_baseurl__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__shared_globaluser__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__providers_questionservice_questionservice__ = __webpack_require__(296);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__providers_answer_service_answer_service__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__providers_comment_service_comment_service__ = __webpack_require__(297);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_list_list__["a" /* ListPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_learn_feed_learn_feed__["a" /* LearnFeedPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_learn_details_learn_details__["a" /* LearnDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_question_details_question_details__["a" /* QuestionDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_manage_question_manage_question__["a" /* ManageQuestionPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_manage_answer_manage_answer__["a" /* ManageAnswerPage */],
                __WEBPACK_IMPORTED_MODULE_16_time_ago_pipe__["a" /* TimeAgoPipe */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/learn-feed/learn-feed.module#LearnFeedPageModule', name: 'LearnFeedPage', segment: 'learn-feed', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/learn-details/learn-details.module#LearnDetailsPageModule', name: 'LearnDetailsPage', segment: 'learn-details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/manage-answer/manage-answer.module#ManageAnswerPageModule', name: 'ManageAnswerPage', segment: 'manage-answer', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/manage-question/manage-question.module#ManageQuestionPageModule', name: 'ManageQuestionPage', segment: 'manage-question', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/question-details/question-details.module#QuestionDetailsPageModule', name: 'QuestionDetailsPage', segment: 'question-details', priority: 'low', defaultHistory: [] }
                    ]
                }),
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_list_list__["a" /* ListPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_learn_feed_learn_feed__["a" /* LearnFeedPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_learn_details_learn_details__["a" /* LearnDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_question_details_question_details__["a" /* QuestionDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_manage_question_manage_question__["a" /* ManageQuestionPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_manage_answer_manage_answer__["a" /* ManageAnswerPage */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_12__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_2__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["c" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_14__providers_process_httpmsg_process_httpmsg__["a" /* ProcessHttpmsgProvider */],
                __WEBPACK_IMPORTED_MODULE_15__providers_learn_service_learn_service__["a" /* LearnServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_18__shared_globaluser__["a" /* GlobalVars */],
                { provide: 'BaseURL', useValue: __WEBPACK_IMPORTED_MODULE_17__shared_baseurl__["a" /* baseURL */] },
                __WEBPACK_IMPORTED_MODULE_19__providers_questionservice_questionservice__["a" /* QuestionserviceProvider */],
                __WEBPACK_IMPORTED_MODULE_20__providers_answer_service_answer_service__["a" /* AnswerServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_21__providers_comment_service_comment_service__["a" /* CommentServiceProvider */],
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 287:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_learn_feed_learn_feed__ = __webpack_require__(109);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_learn_feed_learn_feed__["a" /* LearnFeedPage */];
        this.initializeApp();
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Home', icon: 'home', component: __WEBPACK_IMPORTED_MODULE_4__pages_learn_feed_learn_feed__["a" /* LearnFeedPage */] }
        ];
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/user/buzz/src/app/app.html"*/'<ion-menu [content]="content">\n  <!-- <ion-header>\n    <ion-toolbar>\n      <ion-title>Menu</ion-title>\n    </ion-toolbar>\n  </ion-header> -->\n\n  <ion-content class="side-menu-gradient">\n   <!-- <div>\n      <label style="color: white;margin-left:80px;">Strength of Myanmar</label>\n   </div> -->\n    <div class="spacer" style="height: 20px;"></div>\n    <ion-list>\n      <button class="side-menu-item" menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n          <ion-icon [name]="p.icon" item-left></ion-icon>\n        {{p.title}}\n      </button>\n    </ion-list>\n  </ion-content>\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"/Users/user/buzz/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 293:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_learn_service_learn_service__ = __webpack_require__(84);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, learnService) {
        this.navCtrl = navCtrl;
        this.learnService = learnService;
        this.categories = new Array();
        this.time = Date.now();
    }
    HomePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.learnService.getCategoriesList()
            .subscribe(function (data) {
            _this.categories = data;
            console.log(data);
        });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/user/buzz/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Home</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-card class="category-concept-card"  *ngFor="let category of categories"\n    (click)="openDetails({ category: category })">\n    <ion-card-header style="background-color: #42A5F5;padding: 8px;">\n      <h2 class="category-title">{{ category.title }}</h2>\n    </ion-card-header>\n    <ion-card-content style="padding-top: 10px;">\n      <!-- <ion-avatar item-start>\n        <img style="width:20px;height:20px;" src="assets/imgs/question1.png">\n      </ion-avatar> -->\n      <ion-row>\n        <ion-col no-padding>\n          <ion-badge class="category-tag" *ngFor="let tag of category.tags" item-right>{{ tag.name }}</ion-badge>\n        </ion-col>\n        <div text-right class="category-time">\n          {{time | timeAgo}}\n        </div>\n      </ion-row>\n    </ion-card-content>\n  </ion-card>\n</ion-content>'/*ion-inline-end:"/Users/user/buzz/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_learn_service_learn_service__["a" /* LearnServiceProvider */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 294:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ListPage = /** @class */ (function () {
    function ListPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        // If we navigated to this page, we will have an item available as a nav param
        this.selectedItem = navParams.get('item');
        // Let's populate this page with some filler content for funzies
        this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
            'american-football', 'boat', 'bluetooth', 'build'];
        this.items = [];
        for (var i = 1; i < 11; i++) {
            this.items.push({
                title: 'Item ' + i,
                note: 'This is item #' + i,
                icon: this.icons[Math.floor(Math.random() * this.icons.length)]
            });
        }
    }
    ListPage_1 = ListPage;
    ListPage.prototype.itemTapped = function (event, item) {
        // That's right, we're pushing to ourselves!
        this.navCtrl.push(ListPage_1, {
            item: item
        });
    };
    ListPage = ListPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-list',template:/*ion-inline-start:"/Users/user/buzz/src/pages/list/list.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>List</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <button ion-item *ngFor="let item of items" (click)="itemTapped($event, item)">\n      <ion-icon [name]="item.icon" item-start></ion-icon>\n      {{item.title}}\n      <div class="item-note" item-end>\n        {{item.note}}\n      </div>\n    </button>\n  </ion-list>\n  <div *ngIf="selectedItem" padding>\n    You navigated here from <b>{{selectedItem.title}}</b>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/user/buzz/src/pages/list/list.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], ListPage);
    return ListPage;
    var ListPage_1;
}());

//# sourceMappingURL=list.js.map

/***/ }),

/***/ 296:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QuestionserviceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_baseurl__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__process_httpmsg_process_httpmsg__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_delay__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_delay___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_delay__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_catch__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_catch__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/*
  Generated class for the LearnServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var QuestionserviceProvider = /** @class */ (function () {
    function QuestionserviceProvider(http, processHttpmsgService) {
        this.http = http;
        this.processHttpmsgService = processHttpmsgService;
        console.log('Hello LearnServiceProvider Provider');
    }
    QuestionserviceProvider.prototype.getQuestionsBySlug = function (postid) {
        var _this = this;
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__shared_baseurl__["a" /* baseURL */] + "posts/getallanswers/" + postid)
            .map(function (res) { return _this.processHttpmsgService.extraData(res).posts; })
            .catch(function (error) { return _this.processHttpmsgService.handleError(error); });
    };
    QuestionserviceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__process_httpmsg_process_httpmsg__["a" /* ProcessHttpmsgProvider */]])
    ], QuestionserviceProvider);
    return QuestionserviceProvider;
}());

//# sourceMappingURL=questionservice.js.map

/***/ }),

/***/ 297:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CommentServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(298);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the CommentServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var CommentServiceProvider = /** @class */ (function () {
    function CommentServiceProvider(http) {
        this.http = http;
        console.log('Hello CommentServiceProvider Provider');
    }
    CommentServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], CommentServiceProvider);
    return CommentServiceProvider;
}());

//# sourceMappingURL=comment-service.js.map

/***/ }),

/***/ 49:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return baseURL; });
var baseURL = "http://52.221.180.34:60009/api/";
//# sourceMappingURL=baseurl.js.map

/***/ }),

/***/ 50:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProcessHttpmsgProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_observable_throw__ = __webpack_require__(258);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_observable_throw___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_observable_throw__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/*
  Generated class for the ProcessHttpmsgProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var ProcessHttpmsgProvider = /** @class */ (function () {
    function ProcessHttpmsgProvider(http) {
        this.http = http;
        console.log('Hello ProcessHttpmsgProvider Provider');
    }
    ProcessHttpmsgProvider.prototype.extraData = function (res) {
        var body = res.json();
        return body || {};
    };
    ProcessHttpmsgProvider.prototype.handleError = function (error) {
        var errMsg;
        if (error instanceof __WEBPACK_IMPORTED_MODULE_2__angular_http__["e" /* Response */]) {
            var body = error.json();
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.log(errMsg);
        return __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].throw(errMsg);
    };
    ProcessHttpmsgProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]])
    ], ProcessHttpmsgProvider);
    return ProcessHttpmsgProvider;
}());

//# sourceMappingURL=process-httpmsg.js.map

/***/ }),

/***/ 52:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GlobalVars; });
var GlobalVars = /** @class */ (function () {
    function GlobalVars() {
        this.myGlobalVar = [];
    }
    GlobalVars.prototype.setMyGlobalVar = function (value) {
        this.myGlobalVar = value;
    };
    GlobalVars.prototype.getMyGlobalVar = function () {
        return this.myGlobalVar;
    };
    return GlobalVars;
}());

//# sourceMappingURL=globaluser.js.map

/***/ }),

/***/ 53:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AnswerServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_baseurl__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__process_httpmsg_process_httpmsg__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_delay__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_delay___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_delay__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_catch__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_catch__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/*
  Generated class for the AnswerServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var AnswerServiceProvider = /** @class */ (function () {
    function AnswerServiceProvider(http, processHttpmsgService) {
        this.http = http;
        this.processHttpmsgService = processHttpmsgService;
        console.log('Hello LearnServiceProvider Provider');
    }
    AnswerServiceProvider.prototype.getAnswersBySlug = function (postid) {
        var _this = this;
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__shared_baseurl__["a" /* baseURL */] + "posts/getallanswers/" + postid)
            .map(function (res) { return _this.processHttpmsgService.extraData(res).posts; })
            .catch(function (error) { return _this.processHttpmsgService.handleError(error); });
    };
    AnswerServiceProvider.prototype.getCommentBySlug = function (postid) {
        var _this = this;
        console.log(postid);
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__shared_baseurl__["a" /* baseURL */] + "/comments/" + postid)
            .map(function (res) { return _this.processHttpmsgService.extraData(res).comments; })
            .catch(function (error) { return _this.processHttpmsgService.handleError(error); });
    };
    AnswerServiceProvider.prototype.postAnswer = function (answerText) {
        var _this = this;
        var header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        header.append('Content-Type', 'application/json');
        header.append('Access-Control-Allow-Origin', '*');
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: header });
        console.log(answerText);
        return this.http
            .post(__WEBPACK_IMPORTED_MODULE_2__shared_baseurl__["a" /* baseURL */] + 'posts/save/', answerText, options)
            .map(function (res) { })
            .catch(function (error) { return _this.processHttpmsgService.handleError(error); });
    };
    AnswerServiceProvider.prototype.positiveVote = function (postParams) {
        var _this = this;
        var header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        header.append('Content-Type', 'application/json');
        header.append('Access-Control-Allow-Origin', '*');
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: header });
        return this.http
            .get(__WEBPACK_IMPORTED_MODULE_2__shared_baseurl__["a" /* baseURL */] + 'posts/upvotes/' + postParams["owneruserid"] + "/" + postParams["postid"] + "/" + postParams["userid"])
            .map(function (res) { console.log(res); })
            .catch(function (error) { return _this.processHttpmsgService.handleError(error); });
    };
    AnswerServiceProvider.prototype.negativeVote = function (postParams) {
        var _this = this;
        var header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        header.append('Content-Type', 'application/json');
        header.append('Access-Control-Allow-Origin', '*');
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: header });
        return this.http
            .get(__WEBPACK_IMPORTED_MODULE_2__shared_baseurl__["a" /* baseURL */] + 'posts/downvotes/' + postParams["owneruserid"] + "/" + postParams["postid"] + "/" + postParams["userid"])
            .map(function (res) { console.log(res); })
            .catch(function (error) { return _this.processHttpmsgService.handleError(error); });
    };
    AnswerServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__process_httpmsg_process_httpmsg__["a" /* ProcessHttpmsgProvider */]])
    ], AnswerServiceProvider);
    return AnswerServiceProvider;
}());

//# sourceMappingURL=answer-service.js.map

/***/ }),

/***/ 84:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LearnServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_baseurl__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__process_httpmsg_process_httpmsg__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_delay__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_delay___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_delay__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_catch__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_catch__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/*
  Generated class for the LearnServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var LearnServiceProvider = /** @class */ (function () {
    function LearnServiceProvider(http, processHttpmsgService) {
        this.http = http;
        this.processHttpmsgService = processHttpmsgService;
        console.log('Hello LearnServiceProvider Provider');
    }
    LearnServiceProvider.prototype.getCategoriesList = function () {
        var _this = this;
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__shared_baseurl__["a" /* baseURL */] + "posts/getallposts/1")
            .map(function (res) { return _this.processHttpmsgService.extraData(res).posts; })
            .catch(function (error) { return _this.processHttpmsgService.handleError(error); });
    };
    LearnServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__process_httpmsg_process_httpmsg__["a" /* ProcessHttpmsgProvider */]])
    ], LearnServiceProvider);
    return LearnServiceProvider;
}());

//# sourceMappingURL=learn-service.js.map

/***/ })

},[212]);
//# sourceMappingURL=main.js.map